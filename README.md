# Hostmaker challenge

SSR application made with create-react-app and cra-universal

----
## How to run?
1. Type in terminal `yarn` to install all dependencies
2. Rename `.env.example` filename to `.env`
3. Provide your configuration options like Google Maps api key in `.env` file
4. Type `yarn dev` command in terminal to run both server and client sides
5. Check result in browser. Client side will use `PORT` option from `.env` file, Server side uses `API_PORT`. Example urls: client `https://localhost:3000`, ssr `https://localhost:3000`

----
## Example .ENV file structure

```
PORT=3000
API_PORT=3001
API_BASE_URL=http://localhost
API_VER_URL=api/v1
PROPERTY_ENDPOINT=properties
AREA_ENDPOINT=area
MAP_KEY=YourMapKey
```

----
## Commands
* `yarn start` - runs app on the client side only
* `yarn build` - runs production build for client side only
* `yarn test` - runs unit tests
* `yarn lint` - runs linter for `.js` files
* `yarn lint:styles` - runs styles linter for styled-components
* `yarn server` - runs server and SSR mode
* `yarn dev` - runs client and server
* `yarn prod` - runs production build for client side and server, more info: [cra-universal](https://www.npmjs.com/package/cra-universal#production)

----
## Third party libraries
* [create-react-app](https://github.com/facebook/create-react-app)
* [cra-universal](https://github.com/antonybudianto/cra-universal)
