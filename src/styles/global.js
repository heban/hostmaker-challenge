import styledNormalize from 'styled-normalize';
import { createGlobalStyle } from 'styled-components';
import colors from './colors';

const GlobalStyles = createGlobalStyle`
  ${styledNormalize}

  html {
    box-sizing: border-box;
    height: 100%;
    font-size: 10px;
  }

  *,
  *::before,
  *::after {
    box-sizing: inherit;
  }

  body {
    margin: 0;
    padding: 0;
    font-family: 'Roboto', sans-serif;
    font-size: 1.6rem;
    background: ${colors.grey2};
    height: 100%;
  }
`;

export default GlobalStyles;
