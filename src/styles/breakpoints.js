export default {
  mobile: '(max-width: 759px)',
  tabletAndUp: '(min-width: 760px)',
  tablet: '(min-width: 760px) && (max-width: 1199px)',
  desktop: '(min-width: 1200px)',
};
