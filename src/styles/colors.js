export default {
  grey1: '#e5e6e6',
  grey2: '#fafafa',
  grey3: '#41464e',
  grey4: '#efefef',
  grey5: '#eee',
  green1: '#59c5c7',
  red1: '#da6767',
  white: '#fff',
  whiteOpacity: 'rgba(255, 255, 255, 0.2)',
};
