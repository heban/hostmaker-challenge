import colors from './colors';
import spaces from './spaces';
import breakpoints from './breakpoints';
import sizes from './sizes';

export {
  colors,
  spaces,
  breakpoints,
  sizes,
};
