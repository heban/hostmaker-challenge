import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import SearchButton from './search-button';

it('renders correctly', () => {
  const component = shallow(<SearchButton />);

  expect(toJson(component)).toMatchSnapshot();
});
