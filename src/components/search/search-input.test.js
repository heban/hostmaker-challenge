import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import { SearchInput } from './search-input';

it('renders correctly', () => {
  const component = shallow(<SearchInput fetchArea={() => {}} />);

  expect(toJson(component)).toMatchSnapshot();
});
