// Core
import React, { Fragment } from 'react';

// Components
import ConnectedSearchInput from './search-input';
import ConnectedMap from '../map/map';

const displayName = 'Search';

const Search = () => (
  <Fragment>
    <ConnectedSearchInput />
    <ConnectedMap />
  </Fragment>
);

Search.displayName = displayName;

export default Search;
