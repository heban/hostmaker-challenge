// Core
import React from 'react';

// Styles
import styled from 'styled-components';
import {
  colors,
  sizes,
  spaces,
  breakpoints,
} from '../../styles';

const displayName = 'SearchButton';

const ButtonWrapper = styled.button`
  width: ${sizes.buttonSearchMobileWidth};
  position: absolute;
  right: 0;
  top: 0;
  bottom: 0;
  border: none;
  background: ${colors.green1};
  padding: ${spaces.buttonSearchMobilePadding};
  vertical-align: middle;
  cursor: pointer;

  &:focus {
    outline: none;
    box-shadow: 0 0 .4rem -.1rem ${colors.grey3};
  }

  @media ${breakpoints.tabletAndUp} {
    width: ${sizes.buttonSearchWidth};
    padding: ${spaces.buttonSearchPadding};
  }
`;

const StyledSVG = styled.svg`
  display: inline-block;

  path {
    stroke: ${colors.white};
    fill: ${colors.white};
  }
`;

const SearchButton = () => (
  <ButtonWrapper type="submit">
    <StyledSVG
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
      x="0px"
      y="0px"
      viewBox="0 0 52.966 52.966"
      xmlSpace="preserve"
    >
      <path d="M51.704,51.273L36.845,35.82c3.79-3.801,6.138-9.041,6.138-14.82c0-11.58-9.42-21-21-21s-21,9.42-21,21s9.42,21,21,21
        c5.083,0,9.748-1.817,13.384-4.832l14.895,15.491c0.196,0.205,0.458,0.307,0.721,0.307c0.25,0,0.499-0.093,0.693-0.279
        C52.074,52.304,52.086,51.671,51.704,51.273z M21.983,40c-10.477,0-19-8.523-19-19s8.523-19,19-19s19,8.523,19,19
        S32.459,40,21.983,40z"
      />
    </StyledSVG>
  </ButtonWrapper>
);

SearchButton.displayName = displayName;

export default SearchButton;
