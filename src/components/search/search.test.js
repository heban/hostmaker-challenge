import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import Search from './search';

it('renders correctly', () => {
  const component = shallow(<Search />);

  expect(toJson(component)).toMatchSnapshot();
});
