// Core
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

// Styles
import styled from 'styled-components';
import {
  colors,
  sizes,
  spaces,
  breakpoints,
} from '../../styles';

// Actions
import Actions from '../../redux/actions';

// Components
import SearchButton from './search-button';

const displayName = 'SearchInput';

const propTypes = {
  fetchArea: PropTypes.func.isRequired,
  loading: PropTypes.bool,
};

const defaultProps = {
  loading: false,
};

const InputWrapper = styled.div`
  max-width: ${sizes.inputSearchWidth};
  position: relative;
  margin: ${spaces.inputSearchMargin};
`;

const StyledInput = styled.input`
  width: 100%;
  border: .3rem solid ${colors.grey1};
  background: ${colors.white};
  color: ${colors.grey3};
  padding: ${spaces.inputSearchMobilePadding};
  font-size: ${sizes.inputSearchFontSize};
  transition: all .2s ease-in-out;

  &:focus {
    outline: none;
    border-color: ${colors.green1};
  }

  @media ${breakpoints.tabletAndUp} {
    padding: ${spaces.inputSearchPadding};
  }
`;

export class SearchInput extends Component {
  onFormSubmit = (e) => {
    e.preventDefault();

    const { fetchArea, loading } = this.props;

    if (loading) return;

    const value = this.inputNode.value.trim();

    if (value) {
      fetchArea(value);
    }
  };

  render() {
    return (
      <InputWrapper>
        <form onSubmit={this.onFormSubmit}>
          <StyledInput type="text" placeholder="Type address..." ref={node => (this.inputNode = node)} />
          <SearchButton />
        </form>
      </InputWrapper>
    );
  }
}

const mapStateToProps = ({ mapArea }) => ({
  loading: mapArea.getAreaAttempting,
});

const mapDispatchToProps = {
  fetchArea: Actions.fetchArea,
};

const ConnectedSearchInput = connect(
  mapStateToProps,
  mapDispatchToProps,
)(SearchInput);

SearchInput.propTypes = propTypes;
SearchInput.defaultProps = defaultProps;
SearchInput.displayName = displayName;

export default ConnectedSearchInput;
