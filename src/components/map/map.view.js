// Core
import React from 'react';
import PropTypes from 'prop-types';
import { withScriptjs, withGoogleMap, GoogleMap } from 'react-google-maps';

const displayName = 'MapView';

const propTypes = {
  zoom: PropTypes.number.isRequired,
  lat: PropTypes.number.isRequired,
  lng: PropTypes.number.isRequired,
};

const CustomGoogleMap = withScriptjs(withGoogleMap(props => (
  <GoogleMap
    defaultZoom={props.zoom}
    center={{
      lat: props.lat,
      lng: props.lng,
    }}
  />
)));

const MapView = props => (
  <CustomGoogleMap
    {...props}
    loadingElement={<div style={{ height: '100%' }} />}
    containerElement={<div style={{ height: '50rem' }} />}
    mapElement={<div style={{ height: '100%' }} />}
  />
);

MapView.propTypes = propTypes;
MapView.displayName = displayName;

export default MapView;
