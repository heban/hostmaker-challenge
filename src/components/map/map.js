// Core
import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

// Components
import Spinner from '../spinner/spinner';
import InfoBox from '../infobox/infobox';
import MapView from './map.view';

// Utils
import { STARTING_POINT } from '../../common/address-area';

const displayName = 'Map';
const mapApiKey = process.env.MAP_KEY;
const locationLat = STARTING_POINT.lat;
const locationLng = STARTING_POINT.lng;

const propTypes = {
  loading: PropTypes.bool,
  errorMessage: PropTypes.string,
  info: PropTypes.string,
  lat: PropTypes.number,
  lng: PropTypes.number,
  match: PropTypes.bool,
};

const defaultProps = {
  loading: false,
  errorMessage: '',
  info: '',
  lat: locationLat,
  lng: locationLng,
  match: false,
};

export const Map = ({
  loading,
  errorMessage,
  info,
  lat,
  lng,
  match,
}) => (
  <Fragment>
    {loading && <Spinner />}
    {errorMessage && <InfoBox type="error" message={errorMessage} />}
    {info && <InfoBox message={info} type={match ? 'success' : 'info'} />}
    <MapView
      zoom={9}
      lat={lat}
      lng={lng}
      googleMapURL={`https://maps.googleapis.com/maps/api/js?key=${mapApiKey}&libraries=geometry`}
    />
  </Fragment>
);

const mapStateToProps = ({ mapArea }) => ({
  loading: mapArea.getAreaAttempting,
  lat: mapArea.lat,
  lng: mapArea.lng,
  errorMessage: mapArea.failure,
  info: mapArea.info,
  match: mapArea.match,
});

const ConnectedMap = connect(
  mapStateToProps,
  null,
)(Map);

Map.propTypes = propTypes;
Map.defaultProps = defaultProps;
Map.displayName = displayName;

export default ConnectedMap;
