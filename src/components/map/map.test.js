import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import { Map } from './map';

const PROPS = {
  loading: false,
  lat: 0,
  lng: 0,
  errorMessage: '',
  info: '',
  match: false,
};

it('renders correctly with loader', () => {
  const component = shallow(
    <Map
      {...PROPS}
      loading
    />,
  );

  expect(component.find('Spinner')).toHaveLength(1);
  expect(toJson(component)).toMatchSnapshot();
});

it('renders correctly with errorMessage', () => {
  const component = shallow(
    <Map
      {...PROPS}
      errorMessage="Oops!"
    />,
  );

  expect(component.find('InfoBox')).toHaveLength(1);
  expect(toJson(component)).toMatchSnapshot();
});

it('renders correctly with only MapView', () => {
  const component = shallow(
    <Map
      {...PROPS}
    />,
  );

  expect(component.find('MapView')).toHaveLength(1);
  expect(component.find('InfoBox')).toHaveLength(0);
  expect(toJson(component)).toMatchSnapshot();
});
