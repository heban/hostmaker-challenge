// Core
import React from 'react';

// Styles
import styled from 'styled-components';
import { colors, spaces, sizes } from '../../styles';

// Components
import Logo from '../logo/logo';

const displayName = 'Header';

const HeaderWrapper = styled.div`
  background-color: ${colors.green1};
  padding: ${spaces.headerPadding};
  height: ${sizes.headerHeight};
  text-align: center;
  overflow: hidden;
`;

const Header = () => (
  <HeaderWrapper>
    <Logo />
  </HeaderWrapper>
);

Header.displayName = displayName;

export default Header;
