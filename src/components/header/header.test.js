import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import Header from './header';

it('renders correctly', () => {
  const component = shallow(<Header />);

  expect(toJson(component)).toMatchSnapshot();
});

it('contains <Logo /> component', () => {
  const component = shallow(<Header />);

  expect(component.find('Logo')).toHaveLength(1);
});
