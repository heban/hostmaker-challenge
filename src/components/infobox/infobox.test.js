import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import InfoBox from './infobox';

it('renders correctly when default type is provided', () => {
  const component = shallow(<InfoBox message="Test info message" />);

  expect(toJson(component)).toMatchSnapshot();
});

it('renders correctly with error message', () => {
  const component = shallow(<InfoBox type="error" message="Test error message" />);

  expect(toJson(component)).toMatchSnapshot();
});

it('renders correctly with success message', () => {
  const component = shallow(<InfoBox type="success" message="Test success message" />);

  expect(toJson(component)).toMatchSnapshot();
});
