// Core
import React from 'react';
import PropTypes from 'prop-types';

// Styles
import styled from 'styled-components';
import { colors, spaces } from '../../styles';

const displayName = 'InfoBox';

const propTypes = {
  message: PropTypes.string.isRequired,
  type: PropTypes.string,
};

const defaultProps = {
  type: 'info',
};

const COLORS = {
  info: colors.grey4,
  error: colors.red1,
  success: colors.green1,
};

const InfoBoxWrapper = styled.p`
  margin: 0;
  padding: ${spaces.infoBoxPadding};
  color: ${({ type }) => (type === 'info' ? colors.grey3 : colors.white)};
  background: ${({ type }) => (COLORS[type])};
`;

const InfoBox = ({ type, message }) => (
  <InfoBoxWrapper type={type}>
    {message}
  </InfoBoxWrapper>
);

InfoBox.propTypes = propTypes;
InfoBox.defaultProps = defaultProps;
InfoBox.displayName = displayName;

export default InfoBox;
