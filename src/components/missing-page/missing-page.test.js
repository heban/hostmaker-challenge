import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import MissingPage from './missing-page';

it('renders correctly', () => {
  const component = shallow(<MissingPage />);

  expect(toJson(component)).toMatchSnapshot();
});
