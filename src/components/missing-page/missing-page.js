// Core
import React from 'react';

// Components
import InfoBox from '../infobox/infobox';

const displayName = 'MissingPage';

const MissingPage = () => (
  <InfoBox message="Oops! This page doesn&apos;t exist!" />
);

MissingPage.displayName = displayName;

export default MissingPage;
