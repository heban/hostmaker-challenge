// Core
import React from 'react';

// Styles
import styled, { keyframes } from 'styled-components';
import { colors, sizes } from '../../styles';

const displayName = 'Spinner';

const rotateAnimation = keyframes`
  from {
    transform: rotate(0deg);
  }

  to {
    transform: rotate(360deg);
  }
`;

const SpinnerWrapper = styled.div`
  text-align: center;
  overflow: hidden;
  border: .8rem solid ${colors.whiteOpacity};
  border-left: .8rem solid ${colors.green1};
  margin: 3.2rem auto;
  position: relative;
  text-indent: -9999rem;
  transform: translateZ(0);
  animation: ${rotateAnimation} 1s infinite linear;

  &,
  &::after {
    border-radius: 50%;
    width: ${sizes.spinnerWidth};
    height: ${sizes.spinnerHeight};
  }
`;

const Spinner = () => (
  <SpinnerWrapper>
    Loading...
  </SpinnerWrapper>
);

Spinner.displayName = displayName;

export default Spinner;
