// Core
import React from 'react';
import { NavLink } from 'react-router-dom';

// Styles
import styled from 'styled-components';
import {
  colors,
  spaces,
  sizes,
  breakpoints,
} from '../../styles';

const displayName = 'Navigation';

const NavigationWrapper = styled.nav`
  padding: ${spaces.navPadding};
  max-width: ${sizes.navWidth};
  margin: 0 auto;
  border-bottom: .3rem solid ${colors.grey3};
`;

const NavigationList = styled.ul`
  padding: ${spaces.navListMobilePadding};
  margin: 0;
  list-style: none;
  height: ${sizes.navListHeight};
  overflow: hidden;

  @media ${breakpoints.tabletAndUp} {
    padding: ${spaces.navListPadding};
  }
`;

const NavigationItem = styled.li`
  display: inline-block;
  text-align: center;

  a {
    display: inline-block;
    background: ${colors.green1};
    text-decoration: none;
    color: ${colors.white};
    padding: ${spaces.navItemMobilePadding};
    transition: all .3s ease-in;
    transform: scale(.98) translate3d(0, .8rem, 0);

    @media ${breakpoints.tabletAndUp} {
      padding: ${spaces.navItemPadding};
      width: ${sizes.navItemWidth};
    }
  }
`;

const activeStyle = {
  background: colors.grey3,
  color: colors.white,
  transform: 'scale(1) translate3d(0, 0, 0)',
};

const Navigation = () => (
  <NavigationWrapper>
    <NavigationList>
      <NavigationItem>
        <NavLink to="/" exact activeStyle={activeStyle}>List of properties</NavLink>
      </NavigationItem>
      <NavigationItem>
        <NavLink to="/search" exact activeStyle={activeStyle}>Search on map</NavLink>
      </NavigationItem>
    </NavigationList>
  </NavigationWrapper>
);

Navigation.displayName = displayName;

export default Navigation;
