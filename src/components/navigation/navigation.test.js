import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import Navigation from './navigation';

it('renders correctly', () => {
  const component = shallow(<Navigation />);

  expect(toJson(component)).toMatchSnapshot();
});
