// Core
import React from 'react';
import PropTypes from 'prop-types';

// Styles
import styled from 'styled-components';
import {
  colors,
  sizes,
  spaces,
  breakpoints,
} from '../../styles';

// Components
import PropertyTableAddress from './property-table-address';

const displayName = 'PropertyTableView';

const propTypes = {
  properties: PropTypes.arrayOf(PropTypes.shape({
    airbnbId: PropTypes.number,
    owner: PropTypes.string,
    incomeGenerated: PropTypes.number,
    address: PropTypes.shape({
      line1: PropTypes.string.isRequired,
      line2: PropTypes.string,
      line3: PropTypes.string,
      line4: PropTypes.string.isRequired,
      postCode: PropTypes.string.isRequired,
      city: PropTypes.string.isRequired,
      country: PropTypes.string.isRequired,
    }).isRequired,
  })).isRequired,
};

const StyledPropertyTable = styled.table`
  width: 100%;
  border: none;
  border-spacing: 0;
`;

const TableHeadRow = styled.tr`
  color: ${colors.grey3};
  text-align: left;
`;

const TableHeadCell = styled.th`
  font-weight: bold;
  font-size: ${sizes.fontMobileTh};
  padding: ${spaces.thMobilePadding};
  width: ${spaces.thMobileWidth};
  border-bottom: .2rem solid ${colors.grey3};

  &:nth-child(2) {
    width: ${spaces.thSecondaryMobileWidth};
  }

  @media ${breakpoints.tabletAndUp} {
    width: ${spaces.thWidth};
    font-size: ${sizes.fontTh};
    padding: ${spaces.thPadding};

    &:nth-child(2) {
      width: ${spaces.thSecondaryWidth};
    }
  }
`;

const TableRow = styled.tr`
  background: ${colors.white};
  color: ${colors.green1};
  transition: all .1s linear;
  box-shadow: none;
  z-index: 1;

  &:hover {
    transform: scale(1.05);
    box-shadow: 0 .1rem .4rem -.1rem ${colors.grey3};
    z-index: 2;
  }

  &:nth-child(odd) {
    background: ${colors.grey2};
    color: ${colors.grey3};
  }
`;

const TableCell = styled.td`
  padding: ${spaces.tdMobilePadding};

  @media ${breakpoints.tabletAndUp} {
    padding: ${spaces.tdPadding};
  }
`;

const EmptyRow = styled.tr`
  background: ${colors.grey2};
  color: ${colors.grey3};
  text-align: center;
`;

const PropertyTableView = ({ properties }) => (
  <StyledPropertyTable>
    <thead>
      <TableHeadRow>
        <TableHeadCell>Owner</TableHeadCell>
        <TableHeadCell>Address</TableHeadCell>
        <TableHeadCell>Income Generated</TableHeadCell>
      </TableHeadRow>
    </thead>
    <tbody>
      {properties.length
        ? properties.map(property => (
          <TableRow key={property.airbnbId}>
            <TableCell>{property.owner}</TableCell>
            <TableCell><PropertyTableAddress address={property.address} /></TableCell>
            <TableCell>{`${property.incomeGenerated}£`}</TableCell>
          </TableRow>
        )) : (
          <EmptyRow>
            <TableCell colSpan={3}>Empty list</TableCell>
          </EmptyRow>
        )
      }
    </tbody>
  </StyledPropertyTable>
);

PropertyTableView.propTypes = propTypes;
PropertyTableView.displayName = displayName;

export default PropertyTableView;
