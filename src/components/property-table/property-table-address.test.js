import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import PropertyTableAddress from './property-table-address';

const ADDRESSES = [
  {
    line1: 'Flat 5',
    line4: '7 Westbourne Terrace',
    postCode: 'W2 3UL',
    city: 'London',
    country: 'U.K.',
  },
  {
    line1: 'Flat 6',
    line2: 'Test field',
    line3: 'Test field 2',
    line4: '7 Westbourne Terrace',
    postCode: 'W2 3UL',
    city: 'London',
    country: 'U.K.',
  },
];

it('renders correctly with 5 paragraphs', () => {
  const component = shallow(
    <PropertyTableAddress
      address={ADDRESSES[0]}
    />,
  );

  expect(toJson(component)).toMatchSnapshot();
});

it('renders correctly with 7 paragraphs', () => {
  const component = shallow(
    <PropertyTableAddress
      address={ADDRESSES[1]}
    />,
  );

  expect(toJson(component)).toMatchSnapshot();
});
