// Core
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

// Actions
import Actions from '../../redux/actions';

// Components
import Spinner from '../spinner/spinner';
import InfoBox from '../infobox/infobox';
import PropertyTableView from './property-table.view';

const displayName = 'PropertyTableContainer';

const propTypes = {
  properties: PropTypes.arrayOf(PropTypes.shape({
    airbnbId: PropTypes.number,
    owner: PropTypes.string,
    incomeGenerated: PropTypes.number,
    address: PropTypes.shape({
      line1: PropTypes.string.isRequired,
      line2: PropTypes.string,
      line3: PropTypes.string,
      line4: PropTypes.string.isRequired,
      postCode: PropTypes.string.isRequired,
      city: PropTypes.string.isRequired,
      country: PropTypes.string.isRequired,
    }).isRequired,
  })).isRequired,
  fetchProperties: PropTypes.func.isRequired,
  errorMessage: PropTypes.string,
  loader: PropTypes.bool,
};

const defaultProps = {
  errorMessage: '',
  loader: true,
};

export class PropertyTable extends Component {
  componentDidMount() {
    const { fetchProperties } = this.props;
    fetchProperties();
  }

  render() {
    const { properties, errorMessage, loader } = this.props;

    if (loader) {
      return <Spinner />;
    }

    if (errorMessage) {
      return <InfoBox type="error" message={errorMessage} />;
    }

    return (
      <PropertyTableView properties={properties} />
    );
  }
}

const mapStateToProps = ({ propertiesList }) => ({
  properties: propertiesList.properties,
  errorMessage: propertiesList.failure,
  loader: propertiesList.getPropertiesAttempting,
});

const mapDispatchToProps = {
  fetchProperties: Actions.fetchProperties,
};

const ConnectedPropertyTable = connect(
  mapStateToProps,
  mapDispatchToProps,
)(PropertyTable);

PropertyTable.propTypes = propTypes;
PropertyTable.defaultProps = defaultProps;
PropertyTable.displayName = displayName;

export default ConnectedPropertyTable;
