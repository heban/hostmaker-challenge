import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import { PropertyTable } from './property-table';

const PROPERTIES = [
  {
    owner: 'carlos',
    address: {
      line1: 'Flat 5',
      line4: '7 Westbourne Terrace',
      postCode: 'W2 3UL',
      city: 'London',
      country: 'U.K.',
    },
    airbnbId: 3512500,
    numberOfBedrooms: 1,
    numberOfBathrooms: 1,
    incomeGenerated: 2000.34,
  },
];

const PROPS = {
  properties: PROPERTIES,
  fetchProperties: () => {},
  loader: false,
  errorMessage: '',
};

it('renders correctly with loader', () => {
  const component = shallow(
    <PropertyTable
      {...PROPS}
      loader
    />,
  );

  expect(component.find('Spinner')).toHaveLength(1);
  expect(toJson(component)).toMatchSnapshot();
});

it('renders correctly with errorMessage', () => {
  const component = shallow(
    <PropertyTable
      {...PROPS}
      errorMessage="Oops!"
    />,
  );

  expect(component.find('InfoBox')).toHaveLength(1);
  expect(toJson(component)).toMatchSnapshot();
});

it('renders correctly with PropertyTableView', () => {
  const component = shallow(
    <PropertyTable
      {...PROPS}
    />,
  );

  expect(component.find('PropertyTableView')).toHaveLength(1);
  expect(toJson(component)).toMatchSnapshot();
});
