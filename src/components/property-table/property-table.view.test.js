import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import PropertyTableView from './property-table.view';

const PROPERTIES = [
  {
    owner: 'carlos',
    address: {
      line1: 'Flat 5',
      line4: '7 Westbourne Terrace',
      postCode: 'W2 3UL',
      city: 'London',
      country: 'U.K.',
    },
    airbnbId: 3512500,
    numberOfBedrooms: 1,
    numberOfBathrooms: 1,
    incomeGenerated: 2000.34,
  },
  {
    owner: 'Mario',
    address: {
      line1: 'Flat 6',
      line4: '7 Westbourne Terrace',
      postCode: 'W2 3UL',
      city: 'London',
      country: 'U.K.',
    },
    airbnbId: 3512501,
    numberOfBedrooms: 1,
    numberOfBathrooms: 1,
    incomeGenerated: 2000.34,
  },
];

it('renders correctly with empty row', () => {
  const component = shallow(
    <PropertyTableView
      properties={[]}
    />,
  );

  expect(toJson(component)).toMatchSnapshot();
});

it('renders correctly with 2 rows', () => {
  const component = shallow(
    <PropertyTableView
      properties={PROPERTIES}
    />,
  );

  expect(toJson(component)).toMatchSnapshot();
});
