// Core
import React from 'react';
import PropTypes from 'prop-types';

// Styles
import styled from 'styled-components';
import { spaces } from '../../styles';

const displayName = 'PropertyTableAddress';

const propTypes = {
  address: PropTypes.shape({
    line1: PropTypes.string.isRequired,
    line2: PropTypes.string,
    line3: PropTypes.string,
    line4: PropTypes.string.isRequired,
    postCode: PropTypes.string.isRequired,
    city: PropTypes.string.isRequired,
    country: PropTypes.string.isRequired,
  }).isRequired,
};

const AddressParagraph = styled.p`
  margin: 0;
  padding: ${spaces.addressPadding};
`;

const fields = [
  'line1',
  'line2',
  'line3',
  'line4',
  'postCode',
  'city',
  'country',
];

const PropertyTableAddress = ({ address }) => (
  <div>
    {fields.map((line) => {
      if (address[line]) {
        return <AddressParagraph key={line}>{address[line]}</AddressParagraph>;
      }
      return null;
    })}
  </div>
);

PropertyTableAddress.propTypes = propTypes;
PropertyTableAddress.displayName = displayName;

export default PropertyTableAddress;
