// import axios from 'axios';
const axios = require('axios');

const apiPort = process.env.API_PORT || 3001;
const apiBaseUrl = process.env.API_BASE_URL || 'http://localhost';
const apiVerUrl = process.env.API_VER_URL || 'api/v1';
const endPoint = process.env.AREA_ENDPOINT || 'area';
const mapKey = process.env.MAP_KEY;

const service = axios.create({
  method: 'post',
  timeout: 5000,
  headers: {
    'Content-Type': 'application/json',
  },
});

const mapService = address => new Promise((resolve, reject) => {
  service(`${apiBaseUrl}:${apiPort}/${apiVerUrl}/${endPoint}`, {
    data: {
      address,
      key: mapKey,
    },
  })
    .then((res) => {
      if (typeof res.data === 'string') {
        try {
          JSON.parse(res.data);
        } catch (error) {
          reject(error);
        }
      }
      resolve(res.data);
    })
    .catch((error) => {
      reject(error);
    });
});

export default mapService;
