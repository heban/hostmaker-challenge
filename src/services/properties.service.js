import axios from 'axios';

const apiPort = process.env.API_PORT || 3001;
const apiBaseUrl = process.env.API_BASE_URL || 'http://localhost';
const apiVerUrl = process.env.API_VER_URL || 'api/v1';
const endPoint = process.env.PROPERTY_ENDPOINT || 'properties';

const service = axios.create({
  method: 'get',
  baseURL: `${apiBaseUrl}:${apiPort}/${apiVerUrl}`,
  url: endPoint,
  timeout: 5000,
  headers: {
    'Content-Type': 'application/json',
  },
});

export default () => new Promise((resolve, reject) => {
  service()
    .then((res) => {
      if (typeof res.data === 'string') {
        try {
          JSON.parse(res.data);
        } catch (error) {
          reject(error);
        }
      }
      resolve(res.data);
    })
    .catch((error) => {
      reject(error);
    });
});
