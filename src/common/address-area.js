/* globals window */

export const STARTING_POINT = {
  lat: 51.5073835,
  lng: -0.1277801,
  radius: 20000,
};

const checkAddressInArea = (location, startingPoint = STARTING_POINT) => {
  if (typeof window !== 'undefined' && window.google) {
    const locationCoords = new window.google.maps.LatLng(location);
    const areaCoords = new window.google.maps.LatLng(startingPoint);
    let match = false;

    const computedDistance = window.google.maps.geometry.spherical.computeDistanceBetween(
      locationCoords,
      areaCoords,
    );

    if (startingPoint.radius > computedDistance) {
      match = true;
    }

    return {
      lat: location.lat,
      lng: location.lng,
      match,
    };
  }
  return undefined;
};

export default checkAddressInArea;
