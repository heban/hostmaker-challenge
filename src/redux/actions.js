import Types from './action-types';
import propertiesService from '../services/properties.service';
import mapService from '../services/map.service';
import checkAddressInArea from '../common/address-area';

// Properties action creators
const getPropertiesAttempt = () => ({ type: Types.GET_PROPERTIES_ATTEMPT });
const getPropertiesSuccess = properties => ({ type: Types.GET_PROPERTIES_SUCCESS, properties });
const getPropertiesFailure = message => ({ type: Types.GET_PROPERTIES_FAILURE, message });

// Map action creators
const getAreaAttempt = () => ({ type: Types.GET_AREA_ATTEMPT });
const getAreaSuccess = area => ({
  type: Types.GET_AREA_SUCCESS,
  lat: area.lat,
  lng: area.lng,
  match: area.match,
  info: area.info,
});
const getAreaFailure = message => ({ type: Types.GET_AREA_FAILURE, message });

// Property action creator for redux-thunk
const fetchProperties = () => (dispatch) => {
  dispatch(getPropertiesAttempt());

  propertiesService()
    .then((res) => {
      dispatch(getPropertiesSuccess(res.data));
    })
    .catch((error) => {
      let message = 'Oops! Something went wrong!';

      if (error.response) {
        message = `Something went wrong! Status ${error.response.status}`;
      }

      dispatch(getPropertiesFailure(message));
    });
};

// Property action creator for redux-thunk
const fetchArea = place => (dispatch) => {
  dispatch(getAreaAttempt());

  mapService(place)
    .then((res) => {
      if (res.data.message) {
        dispatch(getAreaFailure(res.data.message));
      } else {
        const data = checkAddressInArea({ lat: res.data.lat, lng: res.data.lng });
        if (data.match) {
          data.info = 'We found your address in our area!';
        } else {
          data.info = 'We did not find your address in our area';
        }

        dispatch(getAreaSuccess(data));
      }
    })
    .catch((error) => {
      let message = 'Oops! Something went wrong!';

      if (error.response) {
        message = `Something went wrong! Status ${error.response.status}`;
      }

      dispatch(getAreaFailure(message));
    });
};

export default {
  getPropertiesAttempt,
  getPropertiesSuccess,
  getPropertiesFailure,
  fetchProperties,
  getAreaAttempt,
  getAreaSuccess,
  getAreaFailure,
  fetchArea,
};
