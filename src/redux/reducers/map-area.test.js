import mapArea from './map-area';
import Types from '../action-types';

it('Action GET_AREA_ATTEMPT returns attempting state', () => {
  const initialState = {
    lat: 0,
    lng: 0,
    match: false,
    getAreaAttempting: false,
    failure: '',
    info: '',
  };

  expect(mapArea(initialState, { type: Types.GET_AREA_ATTEMPT })).toEqual({
    ...initialState,
    getAreaAttempting: true,
  });
});

it('Action GET_AREA_SUCCESS returns success state', () => {
  const initialState = {
    lat: 0,
    lng: 0,
    match: false,
    getAreaAttempting: true,
    failure: '',
    info: '',
  };

  const area = {
    lat: 1,
    lng: 1,
    match: true,
    info: 'Test',
  };

  expect(mapArea(initialState, { type: Types.GET_AREA_SUCCESS, ...area })).toEqual({
    ...area,
    getAreaAttempting: false,
    failure: '',
  });
});

it('Action GET_AREA_FAILURE returns failure state', () => {
  const initialState = {
    lat: 0,
    lng: 0,
    match: false,
    getAreaAttempting: true,
    failure: '',
    info: '',
  };

  expect(mapArea(initialState, { type: Types.GET_AREA_FAILURE, message: 'failure' })).toEqual({
    ...initialState,
    getAreaAttempting: false,
    failure: 'failure',
  });
});
