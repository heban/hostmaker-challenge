import propertiesList from './properties-list';
import Types from '../action-types';

it('Action GET_PROPERTIES_ATTEMPT returns attempting state', () => {
  const initialState = {
    properties: [],
    getPropertiesAttempting: false,
    failure: 'MSG',
  };

  expect(propertiesList(initialState, { type: Types.GET_PROPERTIES_ATTEMPT })).toEqual({
    properties: [],
    getPropertiesAttempting: true,
    failure: '',
  });
});

it('Action GET_PROPERTIES_SUCCESS returns success state', () => {
  const initialState = {
    properties: [],
    getPropertiesAttempting: true,
    failure: 'MSG',
  };

  const properties = 'TEST_PROPERTIES';

  expect(propertiesList(initialState, { type: Types.GET_PROPERTIES_SUCCESS, properties })).toEqual({
    properties,
    getPropertiesAttempting: false,
    failure: '',
  });
});

it('Action GET_PROPERTIES_FAILURE returns failure state', () => {
  const initialState = {
    properties: ['TEST_PROPERTIES'],
    getPropertiesAttempting: true,
    failure: '',
  };

  expect(propertiesList(initialState, { type: Types.GET_PROPERTIES_FAILURE, message: 'failure' })).toEqual({
    properties: ['TEST_PROPERTIES'],
    getPropertiesAttempting: false,
    failure: 'failure',
  });
});
