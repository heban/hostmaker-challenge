import Types from '../action-types';

// Default state
export const initialState = {
  properties: [],
  getPropertiesAttempting: true,
  failure: '',
};

// Reducer
export default (state = initialState, action = {}) => {
  switch (action.type) {
    case Types.GET_PROPERTIES_ATTEMPT:
      return {
        ...state,
        getPropertiesAttempting: true,
        failure: '',
      };
    case Types.GET_PROPERTIES_SUCCESS:
      return {
        ...state,
        properties: action.properties,
        getPropertiesAttempting: false,
        failure: '',
      };
    case Types.GET_PROPERTIES_FAILURE:
      return {
        ...state,
        getPropertiesAttempting: false,
        failure: action.message,
      };
    default:
      return state;
  }
};
