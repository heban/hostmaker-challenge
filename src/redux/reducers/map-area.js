import Types from '../action-types';

// Utils
import { STARTING_POINT } from '../../common/address-area';

// Default state
export const initialState = {
  lat: STARTING_POINT.lat,
  lng: STARTING_POINT.lng,
  match: false,
  getAreaAttempting: false,
  failure: '',
  info: '',
};

// Reducer
export default (state = initialState, action = {}) => {
  switch (action.type) {
    case Types.GET_AREA_ATTEMPT:
      return {
        ...state,
        getAreaAttempting: true,
        failure: '',
        info: '',
      };
    case Types.GET_AREA_SUCCESS:
      return {
        ...state,
        lat: action.lat,
        lng: action.lng,
        match: action.match,
        getAreaAttempting: false,
        failure: '',
        info: action.info,
      };
    case Types.GET_AREA_FAILURE:
      return {
        ...state,
        getAreaAttempting: false,
        failure: action.message,
        info: '',
      };
    default:
      return state;
  }
};
