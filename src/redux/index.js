import { combineReducers } from 'redux';
import propertiesList from './reducers/properties-list';
import mapArea from './reducers/map-area';

export default combineReducers({
  propertiesList,
  mapArea,
});
