// Core
import React, { Fragment } from 'react';
import { Route, Switch } from 'react-router-dom';

// Styles
import styled from 'styled-components';
import {
  spaces,
  sizes,
  colors,
  breakpoints,
} from './styles';
import GlobalStyles from './styles/global';

// Components
import Header from './components/header/header';
import Navigation from './components/navigation/navigation';
import PropertyList from './components/property-table/property-table';
import Search from './components/search/search';
import MissingPage from './components/missing-page/missing-page';

const Content = styled.main`
  padding: ${spaces.mainMobilePadding};
  max-width: ${sizes.mainWidth};
  margin: ${spaces.mainMargin};
  background: ${colors.white};
  border: .1rem solid ${colors.grey4};
  border-top: none;
  box-shadow: 0 .2rem .4rem -.1rem ${colors.grey5};

  @media ${breakpoints.tabletAndUp} {
    padding: ${spaces.mainPadding};
  }
`;

const App = () => (
  <Fragment>
    <Header />
    <Navigation />
    <Content>
      <Switch>
        <Route path="/" exact component={PropertyList} />
        <Route path="/search" exact component={Search} />
        <Route component={MissingPage} />
      </Switch>
    </Content>
    <GlobalStyles />
  </Fragment>
);

export default App;
