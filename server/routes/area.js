const express = require('express');
const request = require('superagent');

const router = express.Router();

router.post('/', (req, res) => {
  request
    .get(`https://maps.googleapis.com/maps/api/geocode/json?key=${req.body.key}&address=${req.body.address}`)
    .then((mapRes) => {
      const { status } = mapRes.body;

      if (status === 'OK') {
        res.json({
          status: 'ok',
          data: mapRes.body.results[0].geometry.location,
        });
      } else {
        res.json({
          status: 'ok',
          data: {
            message: 'We did not find your address in our area',
          },
        });
      }
    })
    .catch((error) => {
      res.status(500).json({ error });
    });
});

module.exports = router;
