const request = require('supertest');
const app = require('../server');
const data = require('../data/properties.json');

describe('GET /api/v1/properties route', () => {
  it('should return OK status', () => {
    request(app)
      .get('/api/v1/properties')
      .then((res) => {
        expect(res.status).toBe(200);
      });
  });

  it('should return properties object', () => {
    request(app)
      .get('/api/v1/properties')
      .expect('Content-Type', /json/)
      .then((res) => {
        expect(res.body.data).toEqual(data);
      });
  });
});
