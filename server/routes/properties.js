const express = require('express');
const propertiesData = require('../data/properties.json');

const router = express.Router();

router.get('/', (req, res) => {
  res.json({
    status: 'ok',
    data: propertiesData,
  });
});

module.exports = router;
