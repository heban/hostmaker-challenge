/* eslint-disable no-console */

// Core
const path = require('path');
const React = require('react');
const universalLoader = require('@cra-express/universal-loader').default;
const staticLoader = require('@cra-express/static-loader').default;
const { renderToNodeStream } = require('react-dom/server');
const { ServerStyleSheet } = require('styled-components');
const { StaticRouter } = require('react-router');
const { Provider } = require('react-redux');
const { createStore } = require('redux');

// Reducers
const reducers = require('../src/redux').default;

// Redux store
const store = createStore(reducers);

// Client app
const { default: App } = require('../src/app');

// Server primary configuration
const server = require('./server');

const clientBuildPath = path.resolve(__dirname, '../client');

let AppClass = App;
let sheet = new ServerStyleSheet();
let jsx = sheet.collectStyles(<AppClass />);

const handleUniversalRender = (req, res) => {
  const context = {};
  const stream = renderToNodeStream(
    <StaticRouter location={req.url} context={context}>
      <Provider store={store}>
        {jsx}
      </Provider>
    </StaticRouter>,
  );

  if (context.url) {
    res.redirect(301, context.url);
    return undefined;
  }

  return stream;
};

// CRA-UNIVERSAL FIX FOR STYLED-COMPONENTS, REDUX AND ROUTING
staticLoader(server, {
  clientBuildPath,
});
universalLoader(server, {
  universalRender: (req, res) => sheet.interleaveWithNodeStream(handleUniversalRender(req, res)),
});

// MODULE HOT REPLACEMENT
if (module.hot) {
  module.hot.accept('../src/app', () => {
    // eslint-disable-next-line global-require, no-shadow
    const { default: App } = require('../src/app');
    AppClass = App;
    sheet = new ServerStyleSheet();
    jsx = sheet.collectStyles(<AppClass />);
    console.log('✅ Server hot reloaded App');
  });
}

module.exports = server;
