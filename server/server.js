/* eslint-disable no-console */

// Core
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const logger = require('morgan');

const app = express();

const properties = require('./routes/properties');
const area = require('./routes/area');

// Useful middlewares
// compression middleware is used directly in cra-universal
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(logger('dev'));

const apiVerUrl = process.env.API_VER_URL || 'api/v1';
const propertyEndPoint = process.env.PROPERTY_ENDPOINT || 'properties';
const areaEndPoint = process.env.PROPERTY_ENDPOINT || 'area';

// Routes
app.use(`/${apiVerUrl}/${propertyEndPoint}`, properties);
app.use(`/${apiVerUrl}/${areaEndPoint}`, area);

module.exports = app;
